
//- Project : KeithleyDMM6500
//- file : KeithleyManager.cpp

#include "task/KeithleyManager.hpp"
#include <iomanip>
#include <yat/utils/XString.h>  //- to_num
#include <yat/utils/String.h>   //- to_upper
#include <yat/Portability.h>    //- NaN
#include <yat/time/Timer.h>     //- perf

// ========================================================
const std::size_t PERIODIC_MSG_PERIOD    = 5000;            //- in ms
const std::size_t MESSAGE_TIMEOUT        = 2500;
//---------------------------------------------------------
//- the YAT user messages
const std::size_t EXEC_LOW_LEVEL_MSG     = yat::FIRST_USER_MSG + 1000;
const std::size_t SET_VOLT_MODE_MSG      = yat::FIRST_USER_MSG + 1010;
const std::size_t SET_TEMP_MODE_MSG      = yat::FIRST_USER_MSG + 1011;
const std::size_t GET_MODE_MSG           = yat::FIRST_USER_MSG + 1012;
const std::size_t SET_NPLC_MSG           = yat::FIRST_USER_MSG + 1020;
const std::size_t GET_NPLC_MSG           = yat::FIRST_USER_MSG + 1021;
const std::size_t GET_RANGE_MSG          = yat::FIRST_USER_MSG + 1030;
const std::size_t SET_AUTO_RANGE_MSG     = yat::FIRST_USER_MSG + 1031;
const std::size_t GET_AUTO_RANGE_MSG     = yat::FIRST_USER_MSG + 1032;
//---------------------------------------------------------
//- configuration
const std::string CMD_VOLTAGE_DC_MODE    = "FUNC \"VOLT:DC\"";
const std::string CMD_TEMPERATURE_MODE   = "FUNC \"TEMP\"";
const std::string CMD_GET_MODE           = "FUNC?";
const std::string VOLT_MODE_PREFIX       = "VOLT:DC";
const std::string TEMP_MODE_PREFIX       = "TEMP";
const Tango::DevDouble MIN_NPLC          = 0.0005;
const Tango::DevDouble MAX_NPLC          = 12;
const Tango::DevDouble DEFAULT_NPLC      = 1.;
const std::string COMMENT                = "#";
//---------------------------------------------------------
//- commands
const std::string CMD_ENABLE_AUTORANGE   = ":RANG:AUTO ON";
const std::string CMD_DISABLE_AUTORANGE  = ":RANG:AUTO OFF";
const std::string CMD_IS_AUTORANGE_ON    = ":RANG:AUTO?";
const std::string CMD_READ_ERROR         = "SYST:ERR?";
const std::string CMD_START_INTEGRATION  = "INIT";
const std::string CMD_STOP_INTEGRATION   = "DCL";           //- device clear message!
const std::string CMD_READ_LAST_VALUE    = "READ?";
const std::string NO_ERROR               = "No error";
//---------------------------------------------------------
const long SLEEP_IN_SECONDS              = 0;
const long SLEEP_BETWEEN_TWO_ACTIONS     = 50000000;        //- 50 milli seconds
//---------------------------------------------------------

namespace KeithleyDMM6500_ns
{
//-----------------------------------------------
//- Ctor ----------------------------------------
KeithleyManager::KeithleyManager (Tango::DeviceImpl * host_device, 
                      std::string proxy_name,
                      std::vector<std::string> config)
: yat4tango::DeviceTask(host_device),
  host_dev (host_device),
  m_proxy_name(proxy_name),
  m_vec_config(config),
  m_com (0),
  m_nplc(DEFAULT_NPLC),
  m_range(""),
  m_autorange_enabled(false),
  m_output_value(yat::IEEE_NAN),
  m_error(NO_ERROR)
{
  DEBUG_STREAM << "KeithleyManager::KeithleyManager <- " << std::endl;
}


//-----------------------------------------------
//- Dtor ----------------------------------------
KeithleyManager::~KeithleyManager (void)
{
  DEBUG_STREAM << "KeithleyManager::~KeithleyManager <- " << std::endl;
  //- Noop
}

//-----------------------------------------------
//- the user core of the Task -------------------
void KeithleyManager::process_message (yat::Message& _msg)
{
  DEBUG_STREAM << "KeithleyManager::handle_message::receiving msg " << _msg.to_string() << std::endl;

  //- handle msg
  switch (_msg.type())
  {
    //- THREAD_INIT =======================
  case yat::TASK_INIT:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message::THREAD_INIT::thread is starting up" << std::endl;
      //- "initialization" code goes here
      try
      {
        m_com.reset(new Communication(host_dev, m_proxy_name));

        m_com->create_proxy();

        //- halt a measurement in progress and place the multimeter in the “idle state.”
        // stop_integration();

        //- send default configuration
        configure();

        //- empty error buffer(if any)
        std::string resp("");
        do
        {
          resp = write_read(CMD_READ_ERROR);
        }
        while(resp.find(NO_ERROR) == std::string::npos);

        //- yat::Task configure optional msg handling
        enable_timeout_msg(false);
        enable_periodic_msg(true);
        set_periodic_msg_period(PERIODIC_MSG_PERIOD);
      }
      catch (...)
      {
        ERROR_STREAM << "KeithleyManager::init_device()*** yat::SocketException caught ***" << std::endl;
        m_com.reset();
      } 
    } 
    break;
    //- TASK_EXIT =======================
  case yat::TASK_EXIT:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling TASK_EXIT thread is quitting" << std::endl;
      
      // stop_integration();

      //- "release" code goes here
      m_com.reset();
    }
    break;
    //- TASK_PERIODIC ===================
  case yat::TASK_PERIODIC:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling TASK_PERIODIC msg " << std::endl;
      //- code relative to the task's periodic job goes here
      try
      {
        read_value();

        get_error();
      }
      catch(...)
      {
        //- CAREFULL:
        //- if NPLC is set to 100, a (CORBA) timeout exception is sent but the HP34401
        //- put the integrated value in its output buffer. Prior to any other command
        //- it is mandatory to read back the output buffer!!!
        //- A sleep of 2 seconds is the minimum to get the integrated value.
        //- This is the reason the exception is not managed here and why a Read command is sent.
        INFO_STREAM << "\n\n\tPERIODIC caugth [....] ; trying READ ..." << std::endl;
        try
        {
          yat::ThreadingUtilities::sleep(2,0);
          //- direct access to HW
          std::string resp = m_com->read();
          {//- critical section
            yat::AutoMutex<> guard(m_data_mutex);

            m_output_value = yat::XString<Tango::DevDouble>::to_num (resp);
          }
          INFO_STREAM << "\t... received [" << resp << "]\n" << std::endl;
        }
        catch(...)
        {
          ERROR_STREAM << "\t ERROR -> PERIODIC caugth [....] ; sending READ ..." << std::endl;
          m_output_value = yat::IEEE_NAN;
        }
      }

    }
    break;
    //- TASK_TIMEOUT ===================
  case yat::TASK_TIMEOUT:
    {
      //- code relative to the task's tmo handling goes here
      ERROR_STREAM << "KeithleyManager::handle_message handling TASK_TIMEOUT msg" << std::endl;
    }
    break;
  //- USER_DEFINED_MSG ================
  //- low level commands (ASCII commands directly passed to the Lakeshore 336 (appending LF at the end)
  case EXEC_LOW_LEVEL_MSG:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling EXEC_LOW_LEVEL_MSG msg" << std::endl;
      // LowLevelMsg * llm = 0;
      // _msg.detach_data(llm);
      // if (llm)
      // {
      //   this->exec_low_level_command (llm->cmd);
      //   delete llm;
      // }
    }
    break;
  case SET_VOLT_MODE_MSG:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling SET_VOLT_MODE_MSG msg" << std::endl;
      static std::string cmd(CMD_VOLTAGE_DC_MODE);

      write_read (cmd);
    }
    break;
  case SET_TEMP_MODE_MSG:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling SET_TEMP_MODE_MSG msg" << std::endl;
      static std::string cmd(CMD_TEMPERATURE_MODE);

      write_read (cmd);
    }
    break;
  case GET_MODE_MSG:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling GET_MODE_MSG msg" << std::endl;
      static std::string cmd(CMD_GET_MODE);
      std::string resp("");

      resp = write_read (cmd);

      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        m_mode = resp;
      }
    }
    break;
  case SET_NPLC_MSG:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling SET_NPLC_MSG msg" << std::endl;
      double* value = 0;
      std::string current_mode("");

      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        current_mode = m_mode;
      }

      _msg.detach_data(value);

      std::stringstream s;
      s << current_mode
        << ":NPLC "
        << *value
        << std::ends;

      write_read (s.str ());
    }
    break;
  case GET_NPLC_MSG:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling GET_NPLC_MSG msg" << std::endl;
      std::string current_mode("");
      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        current_mode = m_mode;
      }
      std::string cmd(current_mode+":NPLC?");
      std::string response("");

      response = write_read (cmd);
      
      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        m_nplc = yat::XString<double>::to_num (response);
      }
    }
    break;
  case GET_RANGE_MSG:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling GET_RANGE_MSG msg" << std::endl;
      std::string current_mode("");

      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        current_mode = m_mode;
      }
      std::string cmd(current_mode+":RANG?");
      std::string response("");
      
      response = write_read (cmd);

      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        m_range = response;
      }
    }
    break;
  case SET_AUTO_RANGE_MSG:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling AUTO_RANGE_MSG msg" << std::endl;
      std::string& value = _msg.get_data<std::string>();
      std::string current_mode("");

      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        current_mode = m_mode;
      }

      std::stringstream s;
      s << current_mode
        << value 
        << std::ends;

      write_read (s.str ());
    }
    break;
  case GET_AUTO_RANGE_MSG:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling AUTO_RANGE_MSG msg" << std::endl;
      std::string current_mode("");

      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        current_mode = m_mode;
      }
      std::string cmd(current_mode + CMD_IS_AUTORANGE_ON);
      std::string response("");

      response = write_read (cmd);
      
      {//- critical section
        yat::AutoMutex<> guard(m_data_mutex);

        m_autorange_enabled = yat::XString<bool>::to_num (response);
      }
    }
    break;
  default:
		  ERROR_STREAM<< "KeithleyManager::handle_message::unhandled msg type received" << std::endl;
		break;
  } //- switch (_msg.type())
} //- KeithleyManager::process_message

//-----------------------------------------------
//- configure HP34401 for acquisition
//-----------------------------------------------
void KeithleyManager::configure()
{
  std::size_t nb_cmd = m_vec_config.size();
  for (std::size_t idx = 0; idx < nb_cmd; idx++)
  {
    //- commands beginning with # are comments
    if (m_vec_config.at(idx).find(COMMENT) != std::string::npos)
    {
      //- skip it
      continue;
    }
    write_read(m_vec_config.at(idx));
  }
}

//-----------------------------------------------
//- set_voltmeter_mode
//-----------------------------------------------
void KeithleyManager::set_voltmeter_mode()
{
  DEBUG_STREAM << "KeithleyManager::set_voltmeter_mode" << std::endl;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(SET_VOLT_MODE_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::set_voltmeter_mode error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::set_voltmeter_mode");
  }

  post(msg);
}

//-----------------------------------------------
//- set_temperature_mode
//-----------------------------------------------
void KeithleyManager::set_temperature_mode()
{
  DEBUG_STREAM << "KeithleyManager::set_temperature_mode" << std::endl;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(SET_TEMP_MODE_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::set_temperature_mode error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::set_temperature_mode");
  }

  post(msg);
}

//-----------------------------------------------
//- get_mode
//-----------------------------------------------
std::string KeithleyManager::get_mode()
{
  bool wait = true;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(GET_MODE_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::get_mode error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::get_mode");
  }

  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_mode;
}

//-----------------------------------------------
//- set_nplc : integration time in NPLC!
//-----------------------------------------------
void KeithleyManager::set_nplc(double nplc)
{
  DEBUG_STREAM << "KeithleyManager::set_nplc" << std::endl;
  if (nplc < 0.0005 || nplc > 12) 
  {
    std::stringstream err;
    err << "KeithleyManager::set_nplc OUT_OF_RANGE : allowed values are ["
        << "0.0005, 12"
        << "]."
        << std::ends;

    ERROR_STREAM << err.str() << std::endl;
    Tango::Except::throw_exception ("OUT_OF_RANGE",
                                    err.str(),
                                    "KeithleyManager::set_nplc");
  }

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(SET_NPLC_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::set_nplc error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::set_nplc");
  }

  msg->attach_data (nplc);
  
  post(msg);
}

//-----------------------------------------------
//- get_nplc
//-----------------------------------------------
double KeithleyManager::get_nplc()
{
  bool wait = true;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(GET_NPLC_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::get_nplc error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::get_nplc");
  }

  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_nplc;
}

//-----------------------------------------------
//- get_range : number of digits
//-----------------------------------------------
std::string KeithleyManager::get_range ()
{
  bool wait = true;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(GET_RANGE_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::get_range error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::get_range");
  }

  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_range;
}

//-----------------------------------------------
//- enable_autorange
//-----------------------------------------------
void KeithleyManager::enable_autorange()
{
  //- prepare msg
  yat::Message * msg = 0;
  msg = new yat::Message(SET_AUTO_RANGE_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::enable_autorange error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::enable_autorange");
  }

  msg->attach_data (CMD_ENABLE_AUTORANGE);
  //-
  post(msg);
}

//-----------------------------------------------
//- disable_autorange
//-----------------------------------------------
void KeithleyManager::disable_autorange()
{
  //- prepare msg
  yat::Message * msg = 0;
  msg = new yat::Message(SET_AUTO_RANGE_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::disable_autorange error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::disable_autorange");
  }

  msg->attach_data (CMD_DISABLE_AUTORANGE);
  //-
  post(msg);
}

//-----------------------------------------------
//- is_autorange_enabled
//-----------------------------------------------
bool KeithleyManager::is_autorange_enabled()
{
  bool wait = true;
  //- prepare msg
  yat::Message * msg = 0;
  msg = new yat::Message(GET_AUTO_RANGE_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::disable_autorange error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::is_autorange_enabled");
  }

  msg->attach_data (CMD_IS_AUTORANGE_ON);
  //- wait till the message is processed !!
  wait_msg_handled(msg, MESSAGE_TIMEOUT);

  yat::AutoMutex<> guard(m_data_mutex);
  return m_autorange_enabled;
}

//-----------------------------------------------
//- returns state/status
//-----------------------------------------------
Tango::DevState KeithleyManager::get_state_status (std::string& status_)
{
  Tango::DevState argout = Tango::UNKNOWN;
  //- error code separator
  std::string separator(",");
  std::string status("");
 //- case of  device proxy error
  if ( !m_com )
  {
    argout  = Tango::FAULT;
    status_ = "Failed to create proxy!";
  }
  else
  {
    {//- critical section
      yat::AutoMutex<> guard(m_data_mutex);

      status = m_error;
    }
    if (status.find(NO_ERROR) == std::string::npos)
    {
      argout = Tango::ALARM;
      status_= status;
    }
    else
    {
      status_= NO_ERROR;
      argout = Tango::ON;
    }
    // status_ = status;
  }

  return argout;
 }

//-----------------------------------------------
//- start_integration
//-----------------------------------------------
void KeithleyManager::start_integration ()
{
  static std::string cmd(CMD_START_INTEGRATION);

  write_read(cmd);
} 
 
//-----------------------------------------------
//- stop_integration
//- NOTE : needs device_clear command from GpibServer
//- which is not implemented yet!!
//-----------------------------------------------
// void KeithleyManager::stop_integration ()
// {
//   static std::string cmd(CMD_STOP_INTEGRATION);

//   write_read(cmd);
// } 
 
//-----------------------------------------------
//- read_value
//-----------------------------------------------
void KeithleyManager::read_value ()
{
  std::string response("");
  static std::string cmd(CMD_READ_LAST_VALUE);

  response = write_read(cmd);

  {//- critical section
    yat::AutoMutex<> guard(m_data_mutex);

    m_output_value = yat::XString<Tango::DevDouble>::to_num (response);
  }
} 

//-----------------------------------------------
//- get_error
//-----------------------------------------------
void KeithleyManager::get_error ()
{
  std::string response("");

  response = write_read(CMD_READ_ERROR);

  {//- critical section
    yat::AutoMutex<> guard(m_data_mutex);

    m_error = response;
  }
}

//-----------------------------------------------
//- write_read
//- send command returns result
//-----------------------------------------------
std::string KeithleyManager::write_read (std::string cmd)
{
  INFO_STREAM << "KeithleyManager::write_read <- for cmd [" << cmd << "]" << std::endl;
  std::string empty("");
  std::string response("");

  //- case of  device proxy error
  if ( !m_com )
  {
    return empty;
  }

  //- check hardware access perf!
  yat::Timer t;

  try
  {
    //- command need a response?
    if ( cmd.rfind("?") != std::string::npos )
    {
      response = m_com->write_read(cmd);
      //- erase last caracter(s) (Line Feed and/or Carriage Return)
      response.erase(response.find("\n"));
    }
    else
    {
      //- only write command
      m_com->write(cmd);
      // INFO_STREAM << "KeithleyManager::write_read() cmd [" << cmd << "] (no response)." << std::endl;
    }
    INFO_STREAM << "KeithleyManager::write_read() cmd [" << cmd 
                << "] response [" << response << "]." 
                << " done in " << t.elapsed_msec() << " milliseconds"
                << std::endl;
    //- Keithley needs few milliseconds between two commands
    yat::ThreadingUtilities::sleep(SLEEP_IN_SECONDS, SLEEP_BETWEEN_TWO_ACTIONS);

  }
  catch (Tango::DevFailed&  df)
  {
    ERROR_STREAM << "KeithleyManager::write_read() -> caugth DevFailed for command [" << cmd << "]." << std::endl;
    ERROR_STREAM << df << std::endl;
    return empty;
  } 
  catch (...)
  {
    ERROR_STREAM << "KeithleyManager::write_read() -> caugth [...] for command [" << cmd << "]." << std::endl;
    return empty;
  }

  return response;
}

//-----------------------------------------------
//- exec_low_level_command
//- sends a Low Level Command (do not check the syntax)  to the hard -blocking!
//-----------------------------------------------
// std::string KeithleyManager::exec_low_level_command (std::string cmd)
// {
//   DEBUG_STREAM << "KeithleyManager::exec_low_level_command for command [" << cmd << "\n]" << std::endl;
//   std::string response;
//   cmd += "\n";
//   //- this is a reading, response expected
//   if (cmd.find ("?") != std::string::npos)
//   {
//     this->write_read (cmd, response);
//     return response;
//   }
//   //- else it is a command, HW does not send a response, return just a " "
//   else
//   {
//     this->write (cmd);
//     return std::string(" ");
//   }
// }

} //- namespace
