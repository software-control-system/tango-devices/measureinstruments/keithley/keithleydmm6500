
//- Project : Keithley Multimeter
//- file : KeithleyManager.hpp
//- threaded reading of the HW


#ifndef __KEITHLEY_MANAGER_H__
#define __KEITHLEY_MANAGER_H__

#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/memory/SharedPtr.h>
#include "task/Communication.hpp"


namespace KeithleyDMM6500_ns
{

//------------------------------------------------------------------------
//- KeithleyManager Class
//- read the HW 
//------------------------------------------------------------------------
class KeithleyManager : public yat4tango::DeviceTask
{
public :

  //- Constructeur/destructeur
  KeithleyManager ( Tango::DeviceImpl * host_device,
              std::string proxy_name,
              std::vector<std::string> config);

  virtual ~KeithleyManager ();

  //- configure Keithley
  void configure ();

  //- the integration time in Number of Power Line Cycle
  void set_nplc (double);
  double get_nplc ();

  //- get output range
  std::string get_range ();

  //- autorange cmds
  void enable_autorange  ();
  void disable_autorange ();
  bool is_autorange_enabled();

  //- modes
  void set_voltmeter_mode();
  void set_temperature_mode();
  std::string get_mode();

  //- returns the HP state and status
  Tango::DevState get_state_status (std::string& status);

  double get_data () {
    yat::AutoMutex<> guard(m_data_mutex);
    return m_output_value;
  }

protected:
  //- process_message (implements yat4tango::DeviceTask pure virtual method)
  virtual void process_message (yat::Message& msg);

  //- mutex
  yat::Mutex m_data_mutex;
  
private :

  //- 
  std::string write_read (std::string);

  //- start integration
  void start_integration();

  //- not implemented yet!
  //- ... need GpibDevice to be changed
  //- stop integration
  // void stop_integration();

  //- read back output value
  void read_value ();

  //- system error
  void get_error ();

  //- the host device 
  Tango::DeviceImpl * host_dev;

  //- communication device proxy name
  std::string m_proxy_name;

  //- config : cmds to send to the Keithley
  std::vector<std::string> m_vec_config;

  //- the communication link
  yat::SharedPtr<Communication> m_com;

  //- Keithley mode
  std::string m_mode;

  //- integration time (Number of Power Line Cycles)
  double m_nplc;

  //- range
  std::string m_range;

  //- autorange?
  bool m_autorange_enabled;

  //- data
  double m_output_value;

  //- error(s)
  std::string m_error;
};

}//- namespace

#endif //- __KEITHLEY_MANAGER_H__
