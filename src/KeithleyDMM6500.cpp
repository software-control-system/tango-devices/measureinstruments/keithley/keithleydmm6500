/*----- PROTECTED REGION ID(KeithleyDMM6500.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        KeithleyDMM6500.cpp
//
// description : C++ source for the KeithleyDMM6500 class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               KeithleyDMM6500 are implemented in this file.
//
// project :     Keithley DMM6500
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include "KeithleyDMM6500.h"
#include "KeithleyDMM6500Class.h"
#include <PogoHelper.h>
#include <yat4tango/Logging.h>
#include <yat4tango/DeviceInfo.h>

/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500.cpp

/**
 *  KeithleyDMM6500 class description:
 *    Class to interface a Keithley Digital Multimeter.
 *    The configuration mode (Voltmeter, Temperature ...), integration time, trigger ... is done in property
 *    using specific (SCPI) commands.
 *    And read back the integrated value in the specified mode.
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name        |  Method name
//================================================================
//  State               |  dev_state
//  Status              |  Inherited (no method)
//  AutorangeON         |  autorange_on
//  AutorangeOFF        |  autorange_off
//  AutorangeEnabled    |  autorange_enabled
//  SetVoltmeterMode    |  set_voltmeter_mode
//  SetTemperatureMode  |  set_temperature_mode
//  GetMode             |  get_mode
//  SetNPLC             |  set_nplc
//  GetNPLC             |  get_nplc
//  GetRange            |  get_range
//================================================================

//================================================================
//  Attributes managed is:
//================================================================
//  outputValue  |  Tango::DevDouble	Scalar
//================================================================

namespace KeithleyDMM6500_ns
{
/*----- PROTECTED REGION ID(KeithleyDMM6500::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : KeithleyDMM6500::KeithleyDMM6500()
 *	Description : Constructors for a Tango device
 *                implementing the classKeithleyDMM6500
 */
//--------------------------------------------------------
KeithleyDMM6500::KeithleyDMM6500(Tango::DeviceClass *cl, string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(KeithleyDMM6500::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::constructor_1
}
//--------------------------------------------------------
KeithleyDMM6500::KeithleyDMM6500(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(KeithleyDMM6500::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::constructor_2
}
//--------------------------------------------------------
KeithleyDMM6500::KeithleyDMM6500(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(KeithleyDMM6500::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : KeithleyDMM6500::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void KeithleyDMM6500::delete_device()
{
	DEBUG_STREAM << "KeithleyDMM6500::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
  m_task_ptr.reset();
  // if(m_task)
  // {
  //   m_task->exit();
  //   m_task = 0;
  // }

  DELETE_SCALAR_ATTRIBUTE(attr_outputValue_read);

  try
  {
    yat4tango::Logging::release(this);
    yat4tango::DeviceInfo::release(this);
  }
  catch(...)
  {
    //- Noop
  }
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::delete_device
}

//--------------------------------------------------------
/**
 *	Method      : KeithleyDMM6500::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void KeithleyDMM6500::init_device()
{
	DEBUG_STREAM << "KeithleyDMM6500::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::init_device_before) ENABLED START -----*/
	
	//	Initialization before get_device_property() call

  yat4tango::DeviceInfo::initialize( this, YAT_XSTR(PROJECT_NAME), YAT_XSTR(PROJECT_VERSION) );
  yat4tango::Logging::initialize(this);
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::init_device_before
	

	//	Get the device properties from database
	get_device_property();
	
	/*----- PROTECTED REGION ID(KeithleyDMM6500::init_device) ENABLED START -----*/
	
	//	Initialize device
  CREATE_SCALAR_ATTRIBUTE(attr_outputValue_read);
	
  //- instanciate and start thread
  try
  {
    if (!communicationLinkName.empty() )
    {
      m_task_ptr.reset(new KeithleyManager(this, communicationLinkName, configuration));
      // m_task = new KeithleyManager( this,
      //                         communicationLinkName,
      //                         configuration);

      if ( m_task_ptr )
      {
        //- start thread
        m_task_ptr->go();

        //- update mode
        m_task_ptr->get_mode();
      }
    }
  }
  catch(...)
  {
    ERROR_STREAM << "init device FAILED : pb DATA task." << std::endl;
    delete_device();
    return;
  }

	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::init_device
}

//--------------------------------------------------------
/**
 *	Method      : KeithleyDMM6500::get_device_property()
 *	Description : Read database to initialize property data members.
 */
//--------------------------------------------------------
void KeithleyDMM6500::get_device_property()
{
	/*----- PROTECTED REGION ID(KeithleyDMM6500::get_device_property_before) ENABLED START -----*/
	
	//	Initialize property data members
  communicationLinkName.clear();
  configuration.clear();
  //- default configuration
  configuration.push_back("#Lines with # are comments!");
  configuration.push_back("#Set TEMPERATURE mode");
  configuration.push_back("FUNC \"TEMP\"");
  configuration.push_back("#Set integration time to 1 nplc");
  configuration.push_back("TEMP:NPLC 1");
  configuration.push_back("#Set precision to 10 digits");
  configuration.push_back("FORMat:ASCii:PRECision 10");

	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::get_device_property_before


	//	Read device properties from database.
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("CommunicationLinkName"));
	dev_prop.push_back(Tango::DbDatum("Configuration"));

	//	is there at least one property to be read ?
	if (dev_prop.size()>0)
	{
		//	Call database and extract values
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);
	
		//	get instance on KeithleyDMM6500Class to get class property
		Tango::DbDatum	def_prop, cl_prop;
		KeithleyDMM6500Class	*ds_class =
			(static_cast<KeithleyDMM6500Class *>(get_device_class()));
		int	i = -1;

		//	Try to initialize CommunicationLinkName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  communicationLinkName;
		else {
			//	Try to initialize CommunicationLinkName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  communicationLinkName;
		}
		//	And try to extract CommunicationLinkName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  communicationLinkName;

		//	Try to initialize Configuration from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  configuration;
		else {
			//	Try to initialize Configuration from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  configuration;
		}
		//	And try to extract Configuration value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  configuration;

	}

	/*----- PROTECTED REGION ID(KeithleyDMM6500::get_device_property_after) ENABLED START -----*/
	
	//	Check device property data members init
	//- Creates default property
  Tango::DbData data_put;

  if ( dev_prop[0].is_empty() )
  {
      Tango::DbDatum  property("CommunicationLinkName");
      property  <<  communicationLinkName;
      data_put.push_back(property);
  }
  if ( dev_prop[1].is_empty() )
  {
      Tango::DbDatum  property("Configuration");
      property  <<  configuration;
      data_put.push_back(property);
  }

  //- write default property if created
  if( !data_put.empty() )
  {
      get_db_device()->put_property(data_put);
  }
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::get_device_property_after
}

//--------------------------------------------------------
/**
 *	Method      : KeithleyDMM6500::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void KeithleyDMM6500::always_executed_hook()
{
	// DEBUG_STREAM << "KeithleyDMM6500::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::always_executed_hook) ENABLED START -----*/
	
	//	code always executed before all requests
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : KeithleyDMM6500::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void KeithleyDMM6500::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	// DEBUG_STREAM << "KeithleyDMM6500::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::read_attr_hardware
}

//--------------------------------------------------------
/**
 *	Read attribute outputValue related method
 *	Description: The current integrated value in the specified mode
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void KeithleyDMM6500::read_outputValue(Tango::Attribute &attr)
{
	// DEBUG_STREAM << "KeithleyDMM6500::read_outputValue(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::read_outputValue) ENABLED START -----*/
  if (m_task_ptr)
  {
      *attr_outputValue_read = m_task_ptr->get_data();
  }
	
	//	Set the attribute value
	attr.set_value(attr_outputValue_read);
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::read_outputValue
}

//--------------------------------------------------------
/**
 *	Method      : KeithleyDMM6500::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void KeithleyDMM6500::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(KeithleyDMM6500::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Command State related method
 *	Description: This command gets the device state (stored in its device_state data member) and returns it to the caller.
 *
 *	@returns Device state
 */
//--------------------------------------------------------
Tango::DevState KeithleyDMM6500::dev_state()
{
	// DEBUG_STREAM << "KeithleyDMM6500::State()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::dev_state) ENABLED START -----*/
	m_status.clear();
	
	Tango::DevState	argout = Tango::UNKNOWN; // replace by your own algorithm
	//	Add your own code
  if ( communicationLinkName.empty() )
  {
    argout   = Tango::FAULT;
    m_status = "CommunicationLinkName property is not defined!";
  }
  else 
  {
    if (m_task_ptr)
    {
      argout = m_task_ptr->get_state_status(m_status);
    }
    else
    {
      argout = Tango::FAULT;
      m_status += "Device initialization failed : no task!";
    }
  }
  set_status(m_status.c_str());
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::dev_state
	set_state(argout);    // Give the state to Tango.
	if (argout!=Tango::ALARM)
		Tango::DeviceImpl::dev_state();
	return get_state();  // Return it after Tango management.
}
//--------------------------------------------------------
/**
 *	Command AutorangeON related method
 *	Description: Enables autorange
 *
 */
//--------------------------------------------------------
void KeithleyDMM6500::autorange_on()
{
	// DEBUG_STREAM << "KeithleyDMM6500::AutorangeON()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::autorange_on) ENABLED START -----*/
	
	//	Add your own code
  if (m_task_ptr)
  {
    m_task_ptr->enable_autorange();
  }
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::autorange_on
}
//--------------------------------------------------------
/**
 *	Command AutorangeOFF related method
 *	Description: Disables autorange
 *
 */
//--------------------------------------------------------
void KeithleyDMM6500::autorange_off()
{
	// DEBUG_STREAM << "KeithleyDMM6500::AutorangeOFF()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::autorange_off) ENABLED START -----*/
	
	//	Add your own code
  if (m_task_ptr)
  {
    m_task_ptr->disable_autorange();
  }
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::autorange_off
}
//--------------------------------------------------------
/**
 *	Command AutorangeEnabled related method
 *	Description: Command to get autorange state
 *
 *	@returns Returns autorange state (ON or OFF)
 */
//--------------------------------------------------------
Tango::DevString KeithleyDMM6500::autorange_enabled()
{
	Tango::DevString argout;
	// DEBUG_STREAM << "KeithleyDMM6500::AutorangeEnabled()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::autorange_enabled) ENABLED START -----*/
	
	//	Add your own code
	std::string is_are("Autorange OFF.");
	//	Add your own code
  if(m_task_ptr)
  {
    bool enabled = m_task_ptr->is_autorange_enabled();
    if(enabled)
    {
      is_are = "Autorange ON.";
    }
	}

  argout = Tango::string_dup(is_are.c_str());
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::autorange_enabled
	return argout;
}
//--------------------------------------------------------
/**
 *	Command SetVoltmeterMode related method
 *	Description: Set the Keithley in Voltmeter mode
 *
 */
//--------------------------------------------------------
void KeithleyDMM6500::set_voltmeter_mode()
{
	// DEBUG_STREAM << "KeithleyDMM6500::SetVoltmeterMode()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::set_voltmeter_mode) ENABLED START -----*/
	
	//	Add your own code
  if (m_task_ptr)
  {
    m_task_ptr->set_voltmeter_mode();
  }
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::set_voltmeter_mode
}
//--------------------------------------------------------
/**
 *	Command SetTemperatureMode related method
 *	Description: Set the Keithley in Temperature mode
 *
 */
//--------------------------------------------------------
void KeithleyDMM6500::set_temperature_mode()
{
	// DEBUG_STREAM << "KeithleyDMM6500::SetTemperatureMode()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::set_temperature_mode) ENABLED START -----*/
	
	//	Add your own code
  if (m_task_ptr)
  {
    m_task_ptr->set_temperature_mode();
  }
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::set_temperature_mode
}
//--------------------------------------------------------
/**
 *	Command GetMode related method
 *	Description: Returns the current mode
 *
 *	@returns The current mode
 */
//--------------------------------------------------------
Tango::DevString KeithleyDMM6500::get_mode()
{
	Tango::DevString argout;
	// DEBUG_STREAM << "KeithleyDMM6500::GetMode()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::get_mode) ENABLED START -----*/
	std::string mode("UNDEFINED");
	//	Add your own code
  if (m_task_ptr)
  {
    mode = m_task_ptr->get_mode();
  }
	
  argout = Tango::string_dup(mode.c_str());
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::get_mode
	return argout;
}
//--------------------------------------------------------
/**
 *	Command SetNPLC related method
 *	Description: Sets the integration time in NPLC value in the range [0.0005, 12] of the current mode.
 *
 *	@param argin The integration time as NPLC value
 */
//--------------------------------------------------------
void KeithleyDMM6500::set_nplc(Tango::DevDouble argin)
{
	// DEBUG_STREAM << "KeithleyDMM6500::SetNPLC()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::set_nplc) ENABLED START -----*/
	
	//	Add your own code
  if (m_task_ptr)
  {
    m_task_ptr->set_nplc(argin);
  }
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::set_nplc
}
//--------------------------------------------------------
/**
 *	Command GetNPLC related method
 *	Description: Returns the NPLC value for the current mode.
 *
 *	@returns The integration time in NPLC
 */
//--------------------------------------------------------
Tango::DevDouble KeithleyDMM6500::get_nplc()
{
	Tango::DevDouble argout;
	// DEBUG_STREAM << "KeithleyDMM6500::GetNPLC()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::get_nplc) ENABLED START -----*/
	
	//	Add your own code
  if (m_task_ptr)
  {
    argout = m_task_ptr->get_nplc();
  }
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::get_nplc
	return argout;
}
//--------------------------------------------------------
/**
 *	Command GetRange related method
 *	Description: Returns the range of the current mode.
 *
 *	@returns Range of the current mode
 */
//--------------------------------------------------------
Tango::DevString KeithleyDMM6500::get_range()
{
	Tango::DevString argout;
	// DEBUG_STREAM << "KeithleyDMM6500::GetRange()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(KeithleyDMM6500::get_range) ENABLED START -----*/
	std::string response("");
	//	Add your own code
  if ( m_task_ptr )
  {
    response = m_task_ptr->get_range();
  }
	
	argout = Tango::string_dup(response.c_str());
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::get_range
	return argout;
}
//--------------------------------------------------------
/**
 *	Method      : KeithleyDMM6500::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void KeithleyDMM6500::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(KeithleyDMM6500::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::add_dynamic_commands
}

/*----- PROTECTED REGION ID(KeithleyDMM6500::namespace_ending) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	KeithleyDMM6500::namespace_ending
} //	namespace
